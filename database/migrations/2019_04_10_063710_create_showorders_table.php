<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('showorders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('u_id');
            $table->string('foods');
            $table->float('total_cost');
            $table->string('destination');
            $table->integer('delivery_time');
            $table->string('served');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('showorders');
    }
}
