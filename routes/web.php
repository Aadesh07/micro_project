<?php

Route::get('/', function () {
    return view('welcome');
});


//test and admins/////////////////////////////////////////////////////////////
Route::get('/test', function(){
    return view('test');
});
Route::post('/testss', 'testController@index');
Route::get('/admin_master', function(){
    return view('admin_add_new');
});
Route::get('/add_new', function(){
    return view('admin_add_new');
});
Route::post('/add_new', 'adminController@add_new');
Route::get('/update', 'adminController@update_food');
Route::get('/update_item/{id}', 'adminController@update_display');
Route::post('/update_item/{id}', 'adminController@edit_item');
Route::get('/taxes', 'adminController@update_tax');
Route::post('/taxes_update', 'adminController@post_tax');
Route::get('/update_users', 'adminController@updateusers');
Route::get('/update_users_re/{id}', 'adminController@updateusers_show');
Route::post('/update_user/{Auth::user()->id}', 'adminController@update_user');
Route::get('/orders', 'adminController@orders');
Route::post('/orders', 'adminController@serve');


//customer end///////////////////////////////////////////////////////////////
Route::get('/login_as_customer', function(){
    return view('login_as_customer');
});

Route::get('/starter_veg', 'foodController@starter_veg');
Route::get('/starter_nonveg', 'foodController@starter_nonveg');
Route::get('/main_veg', 'foodController@main_veg');
Route::get('/main_nonveg', 'foodController@main_nonveg');
Route::get('/dessert', 'foodController@dessert');
Route::post('/order_post', 'foodController@order');
Route::get('/checkout/{id}', 'foodController@checkout');
Route::post('/confirm', 'foodController@confirm');



/////////////////////////////////////////////////////////////////////////////
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
