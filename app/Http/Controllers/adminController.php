<?php

namespace App\Http\Controllers;

use App\Showorder;
use DummyFullModelClass;
use App\lain;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Food;
use App\Tax;
use App\User;
use App\Orderconfirm;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index(lain $lain)
    {

    }

    public function add_new(Request $request)
    {
        $image = $request->file('image');
        $new_image_name = rand(). '.' . $image->getClientOriginalExtension();
        $image->move(public_path("images"), $new_image_name);
        $data = new Food;
        $data->I_name = $request->I_name;
        $data->image = $new_image_name;
        $data->category = $request->category;
        $data->type = $request->type;
        $data->price = $request->price;
        $data->save();
        $request->session()->put('added_new', 'yes');
        return view('admin_add_new');
    }

    public function update_food(){
        $data = Food::all();
        return view('foods_view', ['data' => $data]);
    }

    public function update_display($id){
        $data = Food::where('f_id', $id)->first();
        return view('edit_item_form', ['data' => $data]);
    }

    public function edit_item(Request $request, $id){
        $data = Food::where('f_id', $id)->first();
        $data->I_name = $request->I_name;
        $data->category = $request->category;
        $data->type = $request->type;
        $data->price = $request->price;
        $data->save();
        $request->session()->put('item_updated', 'yes');
        return view('foods_view', ['data' => Food::all()]);
    }

    public function update_tax(){
        $data = Tax::where('id', 1)->first();
        return view('vat_service', ['data' => $data]);
    }

    public function post_tax(Request $request){
        $data = Tax::where('id', 1)->first();
        $data->VAT = $request->VAT;
        $data->service = $request->service;
        $data->discount = $request->discount;
        $data->save();
        $request->session()->put('vat_session', 'yes');
        return view('vat_service', ['data' => $data]);
    }

    public function updateusers(){
        $data = User::all();
        return view('update_users', ['data' => $data]);
    }

    public function updateusers_show($id){
        $data = User::where('id', $id)->first();
        return view('update_users_edit', ['data' => $data]);
    }

    public function update_user(Request $request, $id){
        $data = User::where('id', $id)->first();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->contact_number = $request->contact_number;
        $data->save();
        $request->session()->put('user_updated', 'yes');
        return view('update_users', ['data' => User::all()]);
    }

    public function orders(){
        $orders = Showorder::all();
        return view('admin_orders', ['orders' => $orders]);
    }

    public function serve(Request $request){
        $orders = Showorder::where('u_id', $request->u_id)->first();
        $orders->served = 'yes';
        $orders->save();
        $data = Showorder::all();
        return view('admin_orders', ['orders' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function create(lain $lain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, lain $lain)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function show(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }
}
