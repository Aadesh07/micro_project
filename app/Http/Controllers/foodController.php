<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\lain;
use Illuminate\Http\Request;
use App\Food;
use App\Order;
use App\Tax;
use App\Orderconfirm;
use App\Showorder;

class foodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index(lain $lain)
    {
        //
    }

    public function starter_veg(){
        $data = Food::where('category', 'starter')->where('type', 'veg')->get();
        return view('home', ['data' => $data]);
    }

    public function starter_nonveg(){
        $data = Food::where('category', 'starter')->where('type', 'non-veg')->get();
        return view('home_snon', ['data' => $data]);
    }

    public function main_veg(){
        $data = Food::where('category', 'main')->where('type', 'veg')->get();
        return view('mveg', ['data' => $data]);
    }

    public function main_nonveg(){
        $data = Food::where('category', 'main')->where('type', 'non-veg')->get();
        return view('mnon', ['data' => $data]);
    }

    public function dessert(){
        $data = Food::where('category', 'dessert')->where('type', 'null')->get();
        return view('dessert', ['data' => $data]);
    }

    public function order(Request $request){
        $view = $request->view;
        $category = $request->category;
        $type = $request->type;
        $u_id = $request->user_id;
        $f_id = $request->food_id;
        $f = Food::where('f_id', $f_id)->first();
        $food = $f->I_name;
        $price = $f->price;
        $data = new Order;
        $data->u_id = $u_id;
        $data->f_id = $f_id;
        $data->food = $food;
        $data->price = $price;
        $data->save();
        $food_type = Food::where('category', $category)->where('type', $type)->get();
        return view($view, ['data' => $food_type]);
    }

    public function checkout($id){
        $data = Order::where('u_id', $id)->get();
        $tax_data = Tax::where('id', 1)->first();
        $total = 0;
        foreach ($data as $datas){
            $total = $total + $datas->price;
        }
        $total_taxes = ($total/100 * $tax_data->VAT) + ($total/100 * $tax_data->service) - ($total/100 * $tax_data->discount);
        $actual_total = $total + $total_taxes;
        return view('checkout', ['data' => $data, 'total_without_tax' => $total, 'total_with_tax' => $actual_total]);
    }

    public function confirm(Request $request){
        $order_data = Order::where('u_id', $request->user_id)->get();
        $foods = "";
        foreach ($order_data as $data){
            $foods = $foods . " " . $data->food;
        }
        $confirm = new Orderconfirm;
        $confirm->u_id = $request->user_id;
        $confirm->foods = $foods;
        $confirm->total_cost = $request->total_price;
        $confirm->destination = $request->destination;
        $confirm->delivery_time = 45;
        $confirm->served = 'no';
        $confirm->save();
        $showorders = new Showorder;
        $showorders->u_id = $request->user_id;
        $showorders->foods = $foods;
        $showorders->total_cost = $request->total_price;
        $showorders->destination = $request->destination;
        $showorders->delivery_time = 45;
        $showorders->served = 'no';
        $showorders->save();
        $request->session()->put('confirmed', 'yes');

        Order::where('u_id', $request->user_id)->delete();
        $data = Food::where('category', 'starter')->where('type', 'veg')->get();

        $confirm_data = Orderconfirm::where('u_id', $request->user_id)->first();
        Orderconfirm::where('u_id', $request->user_id)->delete();
        return view('home', ['data' => $data, 'confirm_data' => $confirm_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function create(lain $lain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, lain $lain)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function show(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }
}
