<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Test;

class testController extends Controller
{
    public function index(Request $request){
        $data = new Test;
        $data->name = $request->name;
        $data->age = $request->age;
        $data->save();
        return view('test');
    }
}
