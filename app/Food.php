<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{

    protected $table='foods';
    protected $primaryKey='f_id';
    public $timestamps=false;
}
