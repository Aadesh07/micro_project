<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Showorder extends Model
{

    protected $table= 'showorders';
    protected $primaryKey = 'id';
}
