@extends('layouts.app')

@section('content')
    @if(session('confirmed')== 'yes')
        {{ session(['confirmed' => 'no']) }}
        <script>
            $(function() {
                $("#myModal").modal();
            });
        </script>
    @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Starter (Veg)</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(isset($data))
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">name</th>
                                <th scope="col">image</th>
                                <th scope="col">price</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($data as $datas)
                            <tr>
                                <th scope="row">{{ $datas->I_name }}</th>
                            <td><img src="{{ asset("images/$datas->image") }}" alt="image" height="150" width="150"></td>
                            <td>{{ $datas->price }}</td>
                            <td><form action="/order_post" method="post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="food_id" value="{{ $datas->f_id }}">
                                <input type="hidden" name="view" value="home">
                                <input type="hidden" name="category" value="starter">
                                <input type="hidden" name="type" value="veg">
                                <button type="submit" class="btn btn-primary">Order</button>
                                {{--<button class="btn btn-primary order" id="{{ Auth::user()->id }}" value="{{ $datas->f_id }}">Order</button><br><br>--}}
                                </form></td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Food is on the way!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        @if(isset($confirm_data))
                        <li>Orders: <br>{{ $confirm_data->foods }}<br></li>
                        <li>Total cost: {{ $confirm_data->total_cost }}<br></li>
                        <li>destination: {{ $confirm_data->destination }}<br></li>
                        <li>delivery time: {{ $confirm_data->delivery_time }} mins<br></li>
                        @endif
                    </ul>
                    Note: It might be a good idea to screenshot this information.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{--<script>--}}
        {{--$(document).ready(function(){--}}
            {{--$('.order').click(function(){--}}
                {{--console.log('clicked!');--}}
                {{--var user_id = this.id;--}}
                {{--var food_id = this.value;--}}
                {{--$.post('/order_post',{--}}
                    {{--_token: "{{ csrf_token() }}",--}}
                    {{--user_id: user_id,--}}
                    {{--food_id: food_id--}}
                {{--},--}}
                {{--function(data, status){--}}
                    {{--console.log(status);--}}
                    {{--console.log(data);--}}
                {{--});--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection
