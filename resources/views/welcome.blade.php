@extends('master')

@section('content')
    <style>
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
        .bg-cover {
            background-image: url('/storage/images/jumbo - Copy.jpg');
        }
    </style>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="/login_as_customer"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Login</a></li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron bg-cover">
        <div class="container text-center">
            <h1>Order at your convenience!</h1>
            <p>Here at restaurant...</p>
        </div>
    </div>
    <footer class="container-fluid text-center">
        <p>About us</p>
    </footer>
@endsection

