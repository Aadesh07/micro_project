@extends('master')


@extends('admin_master')
@section('content_admin')
    <div class="container">
        <h2>Update User</h2>
        <form class="form-horizontal" action="/update_user/{{ $data->id }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
                <label class="control-label col-sm-2">Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="U_name" name="name" value="{{ $data->name }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Email:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="email" value="{{ $data->email }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Contact Number:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="contact_number" value="{{ $data->contact_number }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
    </div>
@stop