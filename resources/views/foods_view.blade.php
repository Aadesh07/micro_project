@extends('master')


@extends('admin_master')
@section('content_admin')
    @if(session('item_updated')== 'yes')
        {{ session(['item_updated' => 'no']) }}
        <script>
            alert('Item updated successfully!');
        </script>
    @endif
    <table class="table">
        <thead>
        <tr>
            <th scope="col">name</th>
            <th scope="col">image</th>
            <th scope="col">category</th>
            <th scope="col">type</th>
            <th scope="col">price</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
    @foreach($data as $datas)
        <tr>
        <th scope="row">{{ $datas->I_name }}</th>
            <td><img src="{{ asset("images/$datas->image") }}" alt="image" height="150" width="150"></td>
            <td>{{ $datas->category }}</td>
            <td>{{ $datas->type }}</td>
            <td>{{ $datas->price }}</td>
            <td><a href="/update_item/{{ $datas->f_id }}"><button class="btn btn-default">Edit</button></a></td>
        </tr>
    @endforeach
        </tbody>
    </table>


@stop