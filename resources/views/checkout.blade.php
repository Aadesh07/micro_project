@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Checkout</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(isset($data))
                            @foreach($data as $datas)
                                {{ $datas->food }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $datas->price }}<br><br>
                            @endforeach
                            total cost: {{ $total_without_tax }}<br><br>
                            total cost with tax: {{ $total_with_tax }}<br><br>
                            <br><br>
                            <form action="/confirm" method="post">
                                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                <input type="hidden" name="total_price" value="{{ $total_with_tax }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Destination:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="destination">
                                    </div>
                                </div>
                            <button type="submit" class="btn btn-primary">Confirm order</button></form><br><br>
                            <button class="btn btn-primary" href="/cancel">Cancel all</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    {{--<div class="container">--}}
        {{--<div class="row justify-content-center">--}}
            {{--<div class="col-md-8">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-header">Map</div>--}}

                    {{--<div class="card-body">--}}


                        {{--<style>--}}
                            {{--#inputs,--}}
                            {{--#errors,--}}
                            {{--#directions {--}}
                                {{--position: absolute;--}}
                                {{--width: 33.3333%;--}}
                                {{--max-width: 300px;--}}
                                {{--min-width: 200px;--}}
                            {{--}--}}

                            {{--#inputs {--}}
                                {{--z-index: 10;--}}
                                {{--top: 10px;--}}
                                {{--left: 10px;--}}
                            {{--}--}}

                            {{--#directions {--}}
                                {{--z-index: 99;--}}
                                {{--background: rgba(0,0,0,.8);--}}
                                {{--top: 0;--}}
                                {{--right: 0;--}}
                                {{--bottom: 0;--}}
                                {{--overflow: auto;--}}
                            {{--}--}}

                            {{--#errors {--}}
                                {{--z-index: 8;--}}
                                {{--opacity: 0;--}}
                                {{--padding: 10px;--}}
                                {{--border-radius: 0 0 3px 3px;--}}
                                {{--background: rgba(0,0,0,.25);--}}
                                {{--top: 90px;--}}
                                {{--left: 10px;--}}
                            {{--}--}}

                        {{--</style>--}}

                        {{--<script src='https://api.mapbox.com/mapbox.js/plugins/mapbox-directions.js/v0.4.0/mapbox.directions.js'></script>--}}
                        {{--<link rel='stylesheet' href='https://api.mapbox.com/mapbox.js/plugins/mapbox-directions.js/v0.4.0/mapbox.directions.css' type='text/css' />--}}

                        {{--<div id='map'></div>--}}
                        {{--<div id='inputs'></div>--}}
                        {{--<div id='errors'></div>--}}
                        {{--<div id='directions'>--}}
                            {{--<div id='routes'></div>--}}
                            {{--<div id='instructions'></div>--}}
                        {{--</div>--}}
                        {{--<script>--}}
                            {{--L.mapbox.accessToken = 'pk.eyJ1IjoiYWQzdXRkNyIsImEiOiJjanU4YzdpeGcwZndvNDVwZGxneG1wMWdxIn0.aOrW7a7ZWU7_AQYh6zUy3A';--}}
                            {{--var map = L.mapbox.map('map', null, {--}}
                                {{--zoomControl: false--}}
                            {{--})--}}
                                {{--.setView([40, -74.50], 9)--}}
                                {{--.addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));--}}

                            {{--// move the attribution control out of the way--}}
                            {{--map.attributionControl.setPosition('bottomleft');--}}

                            {{--// create the initial directions object, from which the layer--}}
                            {{--// and inputs will pull data.--}}
                            {{--var directions = L.mapbox.directions();--}}

                            {{--var directionsLayer = L.mapbox.directions.layer(directions)--}}
                                {{--.addTo(map);--}}

                            {{--var directionsInputControl = L.mapbox.directions.inputControl('inputs', directions)--}}
                                {{--.addTo(map);--}}

                            {{--var directionsErrorsControl = L.mapbox.directions.errorsControl('errors', directions)--}}
                                {{--.addTo(map);--}}

                            {{--var directionsRoutesControl = L.mapbox.directions.routesControl('routes', directions)--}}
                                {{--.addTo(map);--}}

                            {{--var directionsInstructionsControl = L.mapbox.directions.instructionsControl('instructions', directions)--}}
                                {{--.addTo(map);--}}
                        {{--</script>--}}


                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
@endsection