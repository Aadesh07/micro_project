@extends('master')


@extends('admin_master')
@section('content_admin')
    <div class="container">
        <h2>Update Item</h2>
        <form class="form-horizontal" action="/update_item/{{ $data->f_id }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
                <label class="control-label col-sm-2">Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="U_name" name="I_name" value="{{ $data->I_name }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Category:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="category" value="{{ $data->category }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Type:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="type" value="{{ $data->type }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Price:</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="price" value="{{ $data->price }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
    </div>
@stop