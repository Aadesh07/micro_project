@extends('master')


@extends('admin_master')
    @section('content_admin')
        @if(session('added_new')== 'yes')
            {{ session(['added_new' => 'no']) }}
            <script>
                alert('New Item added!');
            </script>
        @endif
        <div class="container">
            <h2>Add New Item</h2>
            <form class="form-horizontal" action="/add_new" method="post" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label class="control-label col-sm-2">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="I_name" name="I_name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Image:</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="image">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Category:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="category" name="category">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Type:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="type" name="type">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Price:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" name="price">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    @stop


