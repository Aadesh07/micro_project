<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/add_new">Add New</a></li>
            <li><a href="/update">Update Existing</a></li>
            <li><a href="/taxes">VAT and Service Tax</a></li>
            <li><a href="/update_users">Update Users</a></li>
            <li><a href="/orders">Orders</a></li>
        </ul>
    </div>
</nav>
@yield('content_admin')