@extends('master')


@extends('admin_master')
@section('content_admin')
    <ul>
        <li>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">foods</th>
                    <th scope="col">serve</th>
                </tr>
                </thead>
                <tbody>
            @foreach($orders as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->foods }}</td>
                <td><form action="/orders" method="post">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <input type="hidden" name="u_id" value="{{ $data->u_id }}">
                    @if($data->served == 'no')
                    <button type="submit" class="btn btn-primary">Served</button>
                    @endif
                    </form></td>
                </tr>
            @endforeach
                </tbody>
            </table>
        </li>
    </ul>
@stop


