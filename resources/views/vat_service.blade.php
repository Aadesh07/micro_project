@extends('master')


@extends('admin_master')
    @section('content_admin')
        @if(session('vat_session')== 'yes')
            {{ session(['vat_session' => 'no']) }}
            <script>
                alert('taxes updated!');
            </script>
        @endif
        <div class="container">
            <h2>Update Tax and Discount</h2>
            <form class="form-horizontal" action="/taxes_update" method="post" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label class="control-label col-sm-2">VAT:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="vat" name="VAT" value="{{ $data->VAT }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">service:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="service" value="{{ $data->service }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">discount:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="discount" name="discount" value="{{ $data->discount }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    @stop
