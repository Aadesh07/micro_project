@extends('master')


@extends('admin_master')
    @section('content_admin')
        @if(session('user_updated')== 'yes')
            {{ session(['user_updated' => 'no']) }}
            <script>
                alert('User updated successfully!');
            </script>
        @endif
        <table class="table">
            <thead>
            <tr>
                <th scope="col">name</th>
                <th scope="col">email</th>
                <th scope="col">contact number</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
        @foreach($data as $datas)
            <tr>
                <th scope="row">{{ $datas->name }}</th>
                <td>{{ $datas->email }}</td>
                <td>{{ $datas->contact_number }}</td>
                <td><a href="/update_users_re/{{ $datas->id }}"><button class="btn btn-default">Edit</button></a></td>
            </tr>
        @endforeach
            </tbody>
        </table>


    @stop