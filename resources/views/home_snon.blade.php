@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Starter (Non-Veg)</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            @if(isset($data))
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">name</th>
                                        <th scope="col">image</th>
                                        <th scope="col">price</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                @foreach($data as $datas)
                                    <tr>
                                        <th scope="row">{{ $datas->I_name }}</th>
                                    <td><img src="{{ asset("images/$datas->image") }}" alt="image" height="150" width="150"></td>
                                    <td>{{ $datas->price }}</td>
                                    <td><form action="/order_post" method="post">
                                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="food_id" value="{{ $datas->f_id }}">
                                        <input type="hidden" name="view" value="home_snon">
                                        <input type="hidden" name="category" value="starter">
                                        <input type="hidden" name="type" value="non-veg">
                                        <button type="submit" class="btn btn-primary">Order</button><br><br>
                                        {{--<button class="btn btn-primary order" id="{{ Auth::user()->id }}" value="{{ $datas->f_id }}">Order</button><br><br>--}}
                                    </form></td>
                                    </tr>
                                @endforeach
                                    </tbody>
                                </table>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
